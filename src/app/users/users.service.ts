import { Injectable }    from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class UsersService {

    constructor (
        private http : Http
    ){}

    getGoals(id: number): Promise<any> {
        const url = `http://localhost:3000/v1.0/users/${id}/goals`;
       // console.log("ithe alo");
        return this.http.get(url).toPromise()
        .then(response => {response.json().data as any; 
        //console.log(response.json().data.goalData); 
        return response.json().data.goalData}) 
        .catch(this.handleError);
    }

    getUserInfo(id: number): Promise<any> {
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Authorization', `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsImlhdCI6MTQ5ODg2NTEyM30.y604DZd1fFsmFBYX4u7boqs72CWpYF9ml28Sck7rqbY`);  
        let options = new RequestOptions({ headers: headers });

        const url = `http://localhost:3000/v1.0/users/${id}`;
        return this.http.get(url,options).toPromise()
        .then(response => { response.json() as any;
        console.log(response);
        return response.json() as any})
        .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occured', error);
        return Promise.reject(error.message || error);
    }
}

