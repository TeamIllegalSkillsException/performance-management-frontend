# Employee Performance Management Frontend

## Getting Started

Start the server with `bash run serve` and go to [http://localhost:4200](http://localhost:4200).

## Project Commands

Run commands with `bash run [command] [options...]`.

| Command | Description | Options
|---|---|---
| `serve` | Start web server | `--port [port]`
| `build` | Make a development build |
| `build:aot` | Make a production build |
| `e2e` | Run end to end tests |
| `lint` | Run linter |
| `format` | Format code |
| `test` | Run test watcher with code coverage |
| `test:single` | Run tests only once with code coverage |

## Angular CLI commands

Run [angular-cli commands](https://github.com/angular/angular-cli/wiki) with `npm run ng -- [command] [options...]`, for example:

```sh
# generating a module
npm run ng -- g module my-module

# generating a component
npm run ng -- g component my-component
```
