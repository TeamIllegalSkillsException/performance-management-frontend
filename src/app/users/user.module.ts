import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SynergModule, TileModule } from 'synerg-components';
import { UsersComponent } from './users.component';

import  {  ChartModule  }  from  'angular2-highcharts';
import  * as highcharts  from  'highcharts';
import  {  HighchartsStatic  }  from  'angular2-highcharts/dist/HighchartsService';
import  {  vdlChartingPalette  }  from  './charting-palette';
import { UsersService } from './users.service';

declare  var  require:  any;

export  function  highchartsFactory() {
  const  hc  =  require('highcharts');
  const  hcMore  =  require('highcharts/highcharts-more');
  hc.setOptions(vdlChartingPalette);
  hcMore(hc);
  return  hc;
}

const routes: Routes = [{
  path: 'users/:id',
  component: UsersComponent
}];

@NgModule({
  imports: [
    CommonModule,
    SynergModule,
    TileModule,
    ChartModule,
    RouterModule.forChild(routes),
  ],
  providers: [UsersService,
    {
      provide:  HighchartsStatic,
      useFactory:  highchartsFactory
    }
  ],
  declarations: [UsersComponent],
  exports: [UsersComponent, RouterModule]
})
export class UserModule { }
