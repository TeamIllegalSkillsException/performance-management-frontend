import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { DashboardService } from './dashboard.service';
import { RatingDetails } from './ratingDetails';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
  public list: Array<Object> = [];
  public employeeNames: Array<string> = [];
  public goalCompleted: Array<number> = [];
  public engagementIndex: Array<number> = [];
  public satisfactionIndex: Array<number> = [];

  public absencesIndex: Array<number> = [];
  public tardinessIndex: Array<number> = [];
  public formModel: any;

  public workIndex: any;
  public topPerformer: Array<RatingDetails> = [];
  public underPerformer: Array<RatingDetails> = [];
  public selectedVal: number;
  public selectedSkill: string;

  public teamMembers: any[];
  public teamSkillDetails: any[];
  public len: number;

  public teamMemberNames: Array<string> = [];
  public selfReview: Array<number> = [];
  public managerReview: Array<number> = [];
  public peerReview: Array<number> = [];

  constructor(private router: Router, private dashboardService: DashboardService) { }

  ngOnInit() {
    this.getTeamDetails(2);
    
    this.list = [
      {
        'name': 'Communication',
        'value': 10
      },
       {
        'name': 'Mastery of Skill',
        'value': 13
      },
      {
        'name': 'Leadership',
        'value': 16
      },
      {
        'name': 'Innovation',
        'value': 19
      },
      {
        'name': 'Presentation',
        'value': 22
      },
      {
        'name': 'Frontend',
        'value': 25
      },
      {
        'name': 'Backend',
        'value': 28
      }
    ];

    this.formModel = {
      selectedItem: {
        'name': 'Innovation',
        'value': 19
      }
    };

    this.getSkillInfo(2);
  }

  columnChartOptions = {};
  columnChartOptions2 = {};
  barChartOptions = {};

  btnClick(empId: number) {
    console.log("Manage button clicked"+empId);
    this.router.navigate(['/users/',empId]);
  };

  getTeamDetails(grpid: number): any[] {
    this.dashboardService.getTeamDetails(grpid).then(response => {
      this.teamMembers = response;
      this.teamData();
    });
    return this.teamMembers;
  }

  teamData() {
    let quantData: Array<any> = [];
    this.len = this.teamMembers.length;
    for (var i = 0; i < this.len; i++) {
      this.employeeNames.push(this.teamMembers[i].firstName);
      quantData = this.teamMembers[i].userQuantitativeData;
      for (var j = 0; j < quantData.length; j++) {
        if (quantData[j].dataIdentifierId == 1) {
          var userGoalData = quantData[j].value * 100;
          this.goalCompleted.push(userGoalData);
        } else if (quantData[j].dataIdentifierId == 9) {
          var workEngagement = quantData[j].value * 100;
          this.engagementIndex.push(workEngagement);
        } else if (quantData[j].dataIdentifierId == 8) {
          this.workIndex = (quantData[j].value * 10).toFixed(1);
          var workSatisfaction = quantData[j].value * 100;
          this.satisfactionIndex.push(workSatisfaction);
        } else if (quantData[j].dataIdentifierId == 6) {
          var absencesVal = quantData[j].value * 100;
          this.absencesIndex.push(absencesVal);
        } else if (quantData[j].dataIdentifierId == 7) {
          var tardinessVal = quantData[j].value * 100;
          this.tardinessIndex.push(tardinessVal);
        }
      }

      if (this.workIndex > 5.0) {
        console.log("Work index" +this.workIndex);
        let topData = new RatingDetails(this.teamMembers[i].userId, this.teamMembers[i].firstName, this.workIndex);
        this.topPerformer.push(topData);
        console.log("top performers data " + this.topPerformer);
      }

      if (this.workIndex < 5.0) {
        let underData = new RatingDetails(this.teamMembers[i].userId, this.teamMembers[i].firstName, this.workIndex);
        this.underPerformer.push(underData);
        console.log("Under performers data "+this.underPerformer);
      }

    }

    this.tableRows = this.topPerformer.map(function (x) {
      return [x.userId, x.firstName, x.rating];
    });

    this.tableRows2 = this.underPerformer.map(function (x) {
      return [x.userId, x.firstName, x.rating];
    });

    this.columnChartOptions = {
      chart: {
        type: 'column',
        style: {
          fontFamily: 'inherit'
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0,
          borderWidth: 0,
          dataLabels: {
            enabled: false
          }
        }
      },
      colors: ['#2f7ed8','#910000','#db9500','#1aadce'],
      series: [{
        name: 'Goals Completed on time',
        data: this.goalCompleted

      }, {
        name: 'Employee workplace engagement mean',
        data: this.engagementIndex

      }, {
        name: 'Work satisfaction index',
        data: this.satisfactionIndex

      }],
      xAxis: {
        categories: this.employeeNames,
        title: {
          text: 'Team Members'
        }
      },
      yAxis: {
        title: {
          text: "Range in %"
        }
      }
    };

    this.columnChartOptions2 = {
      chart: {
        type: 'column',
        style: {
          fontFamily: 'inherit'
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0,
          borderWidth: 0,
          dataLabels: {
            enabled: false
          }
        }
      },
      series: [{
        name: 'Absences',
        data: this.goalCompleted

      }, {
        name: 'Tardiness',
        data: this.engagementIndex

      }],
      xAxis: {
        categories: this.employeeNames,
        title: {
          text: 'Team Members'
        }
      },
      yAxis: {
        title: {
          text: "Range in %"
        }
      }
    };
  }

  getSkillInfo(grpid: number): any[] {
    console.log("Value selected in the dropdown "+this.formModel.selectedItem.value+" "+"selected name"+this.formModel.selectedItem.name);
    this.dashboardService.getTeamDetails(grpid).then(response => {
      this.teamSkillDetails = response;
      this.skillsData();
    });
    return this.teamSkillDetails;
  }

  skillsData() {
    console.log("In skills data function");
    let quantData: Array<any> = [];
    this.len = this.teamSkillDetails.length;

    for (var i = 0; i < this.len; i++) {
      this.teamMemberNames.push(this.teamSkillDetails[i].firstName);
      quantData = this.teamSkillDetails[i].userQuantitativeData;

      for (var j = 0; j < quantData.length; j++) {
        console.log("Skill selected "+this.formModel.selectedItem.name);
        var itemVal = parseInt(this.formModel.selectedItem.value);
        if (quantData[j].dataIdentifierId == itemVal) {
          var selfRating = quantData[j].value * 100;
          this.selfReview.push(selfRating);
                    console.log("Data in self review array"+this.selfReview);

        }else if (quantData[j].dataIdentifierId == itemVal+1) {
          var managerRating = quantData[j].value * 100;
          this.managerReview.push(managerRating);
                    console.log("Data in manager review array"+this.managerReview);

        }else if (quantData[j].dataIdentifierId == itemVal+2) {
          var peerRating = quantData[j].value * 100;
          this.peerReview.push(peerRating);
          console.log("Data in peer review array"+this.peerReview);
        }
      }
    }

    this.barChartOptions = {
      chart: {
        type: 'bar',
        style: {
          fontFamily: 'inherit'
        }
      },
      tooltip: {
        valueSuffix: '%'
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: false
          },
          pointWidth: 12,
          pointPadding: 0
        },
        series: {
          borderWidth: 0
        }
      },
      series: [{
        name: 'Self Review',
        data: this.selfReview
      }, {
        name: 'Manager Review',
        data: this.managerReview
      }, {
        name: 'Peer Review',
        data: this.peerReview
      }],
      xAxis: {
        categories: this.teamMemberNames,
        title: {
          text: 'Team Members'
        }
      },
      yAxis: {
        title: {
          text: "Range in %"
        }
      }
    };
  }

  @Input() tableGridTypes: Object[] = [
    { value: 'table', label: 'table' }
  ];

  tableGridDemo = {
    type: this.tableGridTypes[0]
  };

  tableHeaders = ['Id', 'Name', 'Rating'];

  tableRows = this.topPerformer.map(function (x) {
    return [x.userId, x.firstName, x.rating];
  });

  tableRows2 = this.underPerformer.map(function (x) {
    return [x.userId, x.firstName, x.rating];
  });
}
