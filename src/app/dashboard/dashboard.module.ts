import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DropdownListModule, TableModule, SynergModule, AvatarModule } from 'synerg-components';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard.component';
import { ChartModule } from 'angular2-highcharts';
import { DashboardService } from './dashboard.service';

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    SynergModule,
    ChartModule,
    FormsModule,
    ReactiveFormsModule,
    DropdownListModule,
    TableModule,
    AvatarModule,
    RouterModule.forChild(routes),
  ],
  providers: [DashboardService],
  declarations: [DashboardComponent],
  exports: [DashboardComponent, RouterModule]
})
export class DashboardModule { }
