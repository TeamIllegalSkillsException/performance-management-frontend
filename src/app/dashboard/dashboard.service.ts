import { Injectable }    from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
//import { TeamDetails } from './teamDetails';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class DashboardService {

    constructor (
        private http : Http
    ){}

    getTeamDetails(gid: number): Promise<any> {
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Authorization', `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsImlhdCI6MTQ5ODg2NTEyM30.y604DZd1fFsmFBYX4u7boqs72CWpYF9ml28Sck7rqbY`);  
        let options = new RequestOptions({ headers: headers });

        const url = `http://localhost:3000/v1.0/groups/${gid}/users/highlights`;
        return this.http.get(url,options).toPromise()
        .then(response => { response.json().users as any[];
        console.log(response);
        return response.json().users as any[]})
        .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occured', error);
        return Promise.reject(error.message || error);
    }

}

