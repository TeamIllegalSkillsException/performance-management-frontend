import { browser, element, by } from 'protractor';

export class TemplatePage {
  navigateTo() {
    return browser.get('/');
  }

  getHeadingText() {
    return element(by.css('.vdl-content__container h2')).getText();
  }
}
