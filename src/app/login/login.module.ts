import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { loginComponent } from './login.component';
import { SynergModule } from 'synerg-components';

const routes: Routes = [
  {
    path: 'login',
    component: loginComponent
  }
];

@NgModule({
  declarations: [
    loginComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    SynergModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [loginComponent]
})
export class loginModule {
}