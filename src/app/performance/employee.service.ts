import { Injectable } from "@angular/core";
import { Headers, Http } from "@angular/http";
import { RequestOptions } from '@angular/http';
import "rxjs/add/operator/toPromise";
import 'rxjs/add/operator/catch';
import {answer} from "./answer";

@Injectable()
export class EmployeeService {

    constructor(private http: Http) { }
    test : any[];
    Employee(): Promise<answer[]> {

        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Authorization', `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsImlhdCI6MTQ5ODg2NTEyM30.y604DZd1fFsmFBYX4u7boqs72CWpYF9ml28Sck7rqbY`);
        let options = new RequestOptions({ headers: headers });
        var response;
        
    //      this.http.get('http://localhost:3000/v1.0/users/', options).subscribe(data => {
    //         this.test = data.json();
    //     });
    //  // console.log( "serviceData = " + JSON.stringify(this.test));
    //     return this.test;
    
    return this.http.get('http://localhost:3000/v1.0/users/', options)
    .toPromise()
    .then(response => response.json() as answer[]);

    }

    postdata(subjectid: number, value1: number, value2: number, value3: number, value4: number) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsImlhdCI6MTQ5ODg2NTEyM30.y604DZd1fFsmFBYX4u7boqs72CWpYF9ml28Sck7rqbY`);
        let options = new RequestOptions({ headers: headers });
      
      var rand = Math.random();  
      var content = JSON.stringify({        
      ownerId: 3,
      subjectId: subjectid,
      title: 'Peer Review',
      reviewType: 'peer',
      reviewItems: [
          { title: "Leadership Skills", description: "Review Skill 1", value: value1, status:"completed" , response:null }, 
          { title: "Interpersonal Skills", description: "Review Skill 2", value: value2, status:"completed" , response:null }, 
          { title: "Technical Skills", description: "Review Skill 3", value: value3, status:"completed" , response:null }, 
          { title: "Team-work Skills", description: "Review Skill 4", value: value4, status:"completed" , response:null }]

    });

return this.http.post('http://localhost:3000/v1.0/reviews/', content, { headers: headers }).toPromise().catch(function (r){ });

}


}