import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { SynergModule, ActionMenuModule, PopoverModule, ButtonModule } from 'synerg-components';
import { DashboardModule } from './dashboard';
import { UserModule } from './users';
import { FaqModule } from './faq';

import { PerformanceModule } from './performance';
import { loginModule } from './login';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
    //component: loginModule
  },
  {
    path: 'dashboard',
    //redirectTo: '/dashboard',
    //pathMatch: 'full'
   component: DashboardModule
  },
  {
    path: 'users/:id',
    component: UserModule
  },
  {
    path: 'faq',
    component: FaqModule
  },
  {
    path: 'performance',
    component: PerformanceModule
  }
];

@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    SynergModule,
    DashboardModule,
    ActionMenuModule, PopoverModule, ButtonModule,
    UserModule,FaqModule,
    UserModule,
    PerformanceModule,
    loginModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
