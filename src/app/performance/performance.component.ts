import { Component, OnInit, Input, ViewChild, ElementRef, HostBinding } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {EmployeeService} from "./employee.service";
import {Headers} from '@angular/http';
import {Http} from '@angular/http';
import { RequestOptions } from '@angular/http';
import {answer} from './answer';
import { ModalService, ModalModel, ActiveModal, ModalOptions, MODAL_ANIMATIONS } from 'synerg-components';
 
@Component({
  selector: 'app-dashboard',
  templateUrl: './performance.component.html',
  styleUrls: ['./performance.component.scss'],
  providers: [EmployeeService, ModalService]
})
export class PerformanceComponent implements OnInit {

  public html: string = require('!!raw-loader!./performance.component.html');
  public tsModule: string = require('!!raw-loader!./performance.module.ts');
  public tsComponent: string = require('!!raw-loader!./performance.component.ts');
  public scss: string = require('!!raw-loader!./performance.component.scss');
  private show: boolean  = false;
  private submit: boolean  = false;
  public results: {};
  ans: Array<answer>;

 constructor(private employeeService:EmployeeService, private http: Http, private modalService: ModalService) {   
  //this.retrieveResponse();
 }

 retrieveResponse() {
  this.employeeService.Employee().then(ans => {this.ans = ans; 
  console.log(JSON.stringify(ans))
  });
  return this.ans;
 }
postdata(subjectid: number, value1: string, value2: string, value3: string, value4: string): void{
    //console.log(value);
    var a = parseInt(value1);
    var b = parseInt(value2);
    var c = parseInt(value3);
    var d = parseInt(value4);
    console.log(a);
    this.employeeService
    .postdata(subjectid, a, b, c, d);
    this.retrieveResponse();

}

 public selectedItems: any[];
  public allItems: any[] = [
    { id: 1 },
    { id: 2 },
    { id: 3 }
  ];
  public allowMultiple = true;
  
 @Input() orientationTypes: Object[] = [
    { value: 'horizontal', label: 'horizontal' },
    { value: 'vertical', label: 'vertical' }
  ];

 @Input() buttonStyles: Object[] = [
    { value: 'primary', label: 'primary' } ];

  tabsDemo = {
    orientation: this.orientationTypes[0],
    customTabItem: false,
    description: 'Secondary text with additional line'
  };

 formModel = {
    selectedItem: null,
    disabled: false,
    readonly: false,
    group: false,
    filter: false
  };

  formModel2 = {
    selectedItem: null,
    disabled: false,
    readonly: false,
    group: false,
    filter: false
  };

  formModel3 = {
    selectedItem: null,
    disabled: false,
    readonly: false,
    group: false,
    filter: false
  };

  formModel4 = {
    selectedItem: null,
    disabled: false,
    readonly: false,
    group: false,
    filter: false
  };

  formModel5 = {
    selectedItem: null,
    disabled: false,
    readonly: false,
    group: false,
    filter: false,
    name: ''
  };
   formModel6 = {
    selectedItem: null,
    disabled: false,
    readonly: false,
    group: false,
    filter: false
  };
  formModel7 = {
    selectedItem: null,
    disabled: false,
    readonly: false,
    group: false,
    filter: false
  };
  formModel8 = {
    selectedItem: null,
    disabled: false,
    readonly: false,
    group: false,
    filter: false
  };
  formModel9 = {
    selectedItem: null,
    disabled: false,
    readonly: false,
    group: false,
    filter: false
  };
formModel10 = {
    selectedItem: null,
    disabled: false,
    readonly: false,
    group: false,
    filter: false
  };
  formModel11 = {};
  formModel12 = {};
  formModel13 = {};
  formModel14 = {};
  formModel15 = {};
  formModel16 = {};
  formModel17 = {};
  formModel18 = {};
  formModel19 = {};
  formModel20 = {};
  formModel21 = {};
  formModel22 = {};
  formModel23 = {};
  formModel24 = {};
  formModel25 = {};
  formModel26 = {};
  formModel27 = {};
  formModel28 = {};
  formModel29 = {};
  formModel30 = {};
  formModel31 = {};
  formModel32 = {};
  formModel33 = {};
  formModel34 = {};
  formModel35 = {};
  formModel36 = {};
  formModel37 = {};
  formModel38 = {};
  formModel39 = {};
  formModel40 = {};
  formModel41 = {};
  formModel42 = {
    selectedItem: null,
    disabled: false,
    readonly: false,
    group: false,
    filter: false,
    name: ''
  };
  formModel43 = {
    selectedItem: null,
    disabled: false,
    readonly: false,
    group: false,
    filter: false,
    name: ''
  };
  formModel44 = {
    selectedItem: null,
    disabled: false,
    readonly: false,
    group: false,
    filter: false
  };
  formModel45 = {
    selectedItem: null,
    disabled: false,
    readonly: false,
    group: false,
    filter: false
  };
  formModel46 = {
    selectedItem: null,
    disabled: false,
    readonly: false,
    group: false,
    filter: false
  };
  formModel47 = {
    selectedItem: null,
    disabled: false,
    readonly: false,
    group: false,
    filter: false
  };
  formModel48 = {
    selectedItem: null,
    disabled: false,
    readonly: false,
    group: false,
    filter: false
  };
  formModel49 = {
    selectedItem: null,
    disabled: false,
    readonly: false,
    group: false,
    filter: false
  };
  formModel50 = {};
  formModel51 = {};
  formModel52 = {};
  formModel53 = {};
  formModel54 = {};
  formModel55 = {};
  formModel56 = {};
  formModel57 = {};
  formModel58 = {};
  formModel59 = {};

   textareaDemo = {
    disabled: false,
    validationError: false,
    sampleText: ''
  };

  textareaDemo2 = {
    disabled: false,
    validationError: false,
    sampleText: ''
  };
   textareaDemo3 = {
    disabled: false,
    validationError: false,
    sampleText: ''
  };

  textareaDemo4 = {
    disabled: false,
    validationError: false,
    sampleText: ''
  };
   buttonDemo = {
    style: this.buttonStyles[0],
    disabled: false,
    busy: false
  };
   buttonDemo2 = {
    style: this.buttonStyles[0],
    disabled: false,
    busy: false
  };
   buttonDemo3 = {
    style: this.buttonStyles[0],
    disabled: false,
    busy: false
  };
  buttonDemo4 = {
    style: this.buttonStyles[0],
    disabled: false,
    busy: false
  };

  public list: Array<Object>;
  public list2: Array<Object>;
  public list3: Array<Object>;
  public list4: Array<Object>;
  public list5: Array<Object>;
  public list6: Array<Object>;
  public list7: Array<Object>;
  public list8: Array<Object>;
  public list9: Array<Object>;
  
  htmlText: string;
  ngAfterViewInit() { }
  addRow() {
    this.show = !this.show;    
  }

submitRow(){
this.submit = !this.submit;
}
private textValue = "";
private log: string ='';
private logText(value: string): void {
        this.log += `${value}\n`
    }

 onChange(str: string) {
  this.employeeService.Employee().then(ans => {this.ans = ans; 
    for(var i = 0; i < ans.length; i++){
      if (str == ans[i].firstName)
        {
          this.empNull = ans[i].goals;
        } 
    }

      });
  }

empNull = {};

  ngOnInit() {
  this.employeeService.Employee().then(ans => {this.ans = ans; 
    //console.log(JSON.stringify(this.ans[0].firstName));
    this.list = [
      {
        'name': 'Need to think about it. Jake does a decent job',
        'value': 'coquelicot'
      },
      {
        'name': 'Let him Go!',
        'value': 'red'
      },
      {
        'name': 'Consider raising his salary/benefits to make him stay',
        'value': 'blue'
      },
      {
        'name': 'Ask for a 2 month prior notice from him, before he plans to leave',
        'value': 'pink'
      }
    ];

     this.list2 = [
      {
        'name': 'Good Engagement and a mostly positive attitude',
        'value': 'coquelicot'
      },
      {
        'name': 'Not much Engaged',
        'value': 'red'
      },
      {
        'name': 'Very Busy most of the time!',
        'value': 'blue'
      },
      {
        'name': 'Partially engaged, most of the time he just has to answer clients phone call for a ticket',
        'value': 'pink'
      }
    ];

    this.list3 = [
      {
        'name': 'I feel I am highly valued',
        'value': 'coquelicot'
      },
      {
        'name': 'I sometimes feel valued',
        'value': 'red'
      },
      {
        'name': 'I am not sure others value, what I do',
        'value': 'blue'
      },
      {
        'name': 'I do not feel valued at ADP',
        'value': 'pink'
      }
    ];

    this.list4 = [
      {
        'name': 'Work that better fits my strengths',
        'value': 'coquelicot'
      },
      {
        'name': 'Performance appraisal from my supervisors',
        'value': 'red'
      },
      {
        'name': 'Flexible office hours and working environment',
        'value': 'blue'
      },
      {
        'name': 'Support of my other team members',
        'value': 'pink'
      }
    ];

this.list5 = [
      {
        'name': ans[0].firstName,
        'value': 'coquelicot'
      },
      {
        'name': ans[1].firstName,
        'value': 'red'
      },
      {
        'name': ans[2].firstName,
        'value': 'blue'
      },
      {
        'name': ans[3].firstName,
        'value': 'pink'
      },
      {
        'name': ans[4].firstName,
        'value': 'black'
      }
    ];

    this.list6 = [
      {
        'name': '0',
        'value': 'coquelicot'
      },
      {
        'name': '1',
        'value': 'red'
      },
      {
        'name': '2',
        'value': 'blue'
      },
      {
        'name': '3',
        'value': 'pink'
      },
      {
        'name': '4',
        'value': 'green'
      },
      {
        'name': '5',
        'value': 'black'
      }
    ];
     this.list7 = [
      {
        'name': '0',
        'value': 'coquelicot'
      },
      {
        'name': '1',
        'value': 'red'
      },
      {
        'name': '2',
        'value': 'blue'
      },
      {
        'name': '3',
        'value': 'pink'
      },
      {
        'name': '4',
        'value': 'green'
      },
      {
        'name': '5',
        'value': 'black'
      }
    ];
    this.list8 = [
      {
        'name': '0',
        'value': 'coquelicot'
      },
      {
        'name': '1',
        'value': 'red'
      },
      {
        'name': '2',
        'value': 'blue'
      },
      {
        'name': '3',
        'value': 'pink'
      },
      {
        'name': '4',
        'value': 'green'
      },
      {
        'name': '5',
        'value': 'black'
      }
    ];
    this.list9 = [
      {
        'name': '0',
        'value': 'coquelicot'
      },
      {
        'name': '1',
        'value': 'red'
      },
      {
        'name': '2',
        'value': 'blue'
      },
      {
        'name': '3',
        'value': 'pink'
      },
      {
        'name': '4',
        'value': 'green'
      },
      {
        'name': '5',
        'value': 'black'
      }
    ];

})
  }  
}

