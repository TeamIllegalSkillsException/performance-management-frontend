import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from './users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})

export class UsersComponent implements OnInit {
  private id : number;
  public fullName: string;
  public mailId: string;
  public golCount: number;
  public completedGols: number;
  public openGols: number;
  public overdueGols: number;
  public goalData: any;
  public userInfo: any;
  public sub: any;
  public defaultId: number = 3;
  public grpName: string;
  public jobTitle: string;

  constructor(private userService: UsersService, private actroute: ActivatedRoute) {
  }

  ngOnInit() {
    this.sub = this.actroute.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });

    this.getGoals(this.id);
    this.getUserInfo(this.id);
  }

  getGoals(uid: number): any {
    this.userService.getGoals(uid).then(response => {
      this.goalData = response;
      //  console.log("component is:" + this.goalData.goalCount);
      this.collectdata();
    });
    return this.goalData;
  }
  donutChartOptions = {};
  donutChartOptions1 = {};

  collectdata() {
    this.golCount = this.goalData.goalCount;
    this.completedGols = this.goalData.completedGoals;
    this.openGols = this.goalData.openGoals;
    this.overdueGols = this.goalData.overdueGoals;

    this.donutChartOptions = {
      chart: {
        type: 'pie',
        style: {
          fontFamily: 'inherit'
        }
      },
      plotOptions: {
        pie: {
          shadow: false,
          innerSize: '60%',
          borderWidth: 2,
          // size: 150
        }
      },
      series: [{
        name: 'Goals',
        data: [
          ["Completed", this.completedGols],
          ["Not Completed", this.golCount]
        ],
        showInLegend: true,
        dataLabels: {
          enabled: true,
          style: {
            font: 'ProximaNova, sans-serif',
            fontSize: '14px',
            fontWeight: '300'
          }
        }
      }],
      credits: {
        enabled: false
      },
      title: {
        text: 'Donut Chart',
        align: 'left',
        x: 20
      },
    }

    this.donutChartOptions1 = {
      chart: {
        type: 'pie',
        style: {
          fontFamily: 'inherit'
        }
      },
      plotOptions: {
        pie: {
          shadow: false,
          innerSize: '60%',
          borderWidth: 2,
          // size: 150
        }
      },
      series: [{
        name: 'Goals',
        data: [
          ["Over Due", this.overdueGols],
          ["Total goals", this.golCount]
        ],
        showInLegend: true,
        dataLabels: {
          enabled: true,
          style: {
            font: 'ProximaNova, sans-serif',
            fontSize: '14px',
            fontWeight: '300'
          }
        }
      }],
      credits: {
        enabled: false
      },
      title: {
        text: 'Donut Chart',
        align: 'left',
        x: 20
      },
    }
  }

  getUserInfo(uid: number): any {
    this.userService.getUserInfo(uid).then(response => {
      this.userInfo = response;
    //  console.log("In user component" +this.userInfo)
     this.userData();
    });
    return this.userInfo;
  }

  userData() {
    this.fullName = this.userInfo.firstName +" " +this.userInfo.lastName;
    this.mailId = this.userInfo.emailAddress;
    this.jobTitle = this.userInfo.title;
    //this.grpName = this.userInfo.groups.title;
    console.log("group name "+this.grpName);
    console.log("In userdata function" +this.fullName);
  }

  lineChartOptions = {
    chart: {
      style: {
        fontFamily: 'inherit'
      },
      width: '1000'
    },
    tooltip: {
      valueSuffix: ' days'
    },
    plotOptions: {
      series: {
        allowPointSelect: true,
        marker: {
          symbol: 'circle',
          radius: 4,
          fillColor: '#ffffff',
          lineColor: null,
          lineWidth: '2',
          states: {
            hover: {
              radius: 4,
              fillColor: '#ffffff',
              lineColor: null,
              lineWidth: '2'
            },
            select: {
              radius: 7,
              fillColor: '#ffffff',
              lineColor: null,
              lineWidth: '2'
            }
          }
        }
      },
      column: {
        allowPointSelect: false,
        borderWidth: 0
      }
    },
    series: [{
      name: 'Sick Leave',
      data: [2, 3, 1, 0, 1, 1, 2, 3, 0, 0, 1, 1]
    }, {
      name: 'Emergency Leave',
      data: [0, 0, 1, 0, 1, 1, 2, 3, 1, 1, 1, 1]
    }, {
      name: 'Casual Leave',
      data: [10, 1, 5, 4, 2, 3, 2, 1, 5, 4, 1, 2]
    }],
    xAxis: {
      //      lineWidth : 100,
      categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    }
  };
}
