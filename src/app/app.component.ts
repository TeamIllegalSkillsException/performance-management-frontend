import { Component, OnInit, HostListener } from '@angular/core';
import { ActionMenuModule, PopoverModule, ButtonModule, IMastheadConfig, IMenuAction, INavigationItem } from 'synerg-components';
import { ActivatedRoute, Router } from '@angular/router';
import { Injectable } from "@angular/core";

export const ANIMATIONS = {
  expand: 'expand',
  contract: 'contract'
};


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
@Injectable()
export class AppComponent implements OnInit {
  public menuState: string = ANIMATIONS.contract;
  public actions: IMenuAction[];
  public navigation: INavigationItem[];
  public searchValue: string;
  public mastheadConfig: IMastheadConfig;
  public title: string;
  public items: any[];
  public mybool: string = "";
constructor (private router: Router) {}

  toggleMenu() {
    this.menuState = this.menuState === ANIMATIONS.contract ?
      ANIMATIONS.expand : ANIMATIONS.contract;
  }

  @HostListener('window:resize')
  onResize() {
    this.menuState = ANIMATIONS.contract;
  }

  ngOnInit() {
    this.mybool = "false";
  
    this.title = 'Dashboard';
    // To hide messages and alerts, omit from the mastheadConfig object
    this.mastheadConfig = {
      avatar: {
        name: 'Daniel Reinke',
        imgPath: 'https://placeimg.com/80/80/people',
      },
      search: true,
      clientLogo: {
        type: 'icon',
        iconClass: 'icon-adp'
      },
      messages: 99,
      alerts: 1,
      actions: [
        {
          name: 'View Profile',
          icon: 'fa-user'
        },
        {
          name: 'Settings',
          icon: 'fa-gear'
        },
        {
          name: 'Tour',
          icon: 'fa-road'
        }
      ]
    };

    this.items = [
      {
        label: 'Dashboard',
        icon: 'tachometer',
        active: true
      },
      {
        label: 'Profile',
        icon: 'users',
        active: false
      },
      {
        label: 'Performance',
        icon: 'tasks',
        active: false
      },
      {
        label: 'Knowledge',
        icon: 'question-circle',
        active: false
      }
    ];
  
  }

  mobileClick() {
    this.toggleMenu();
  }


  getAction(action: IMenuAction) {
    console.log(action.name + ' clicked.');
  }

  searchChange($event: Event) {
    let target = $event.target as HTMLInputElement;
    this.searchValue = target.value;
    console.log(this.searchValue);
  }

  searchClick() {
    alert('Search: ' + this.searchValue);
  }

  menuClick($event: Event) {
  
    let target = $event.target as HTMLInputElement;
    let value = target.value;
    let currentTarget = $event.currentTarget as HTMLOutputElement;
    console.log("currentTarget = " + currentTarget.innerText);
    
    if((currentTarget.innerText.charAt(1)) == 'r') {
        this.title = "Profile";
        this.router.navigate(['/users/1']);
    }else if((currentTarget.innerText.charAt(0)) == 'K'){
        this.title = "Knowledge";
        this.router.navigate(['/faq']);
    }
    else if((currentTarget.innerText.charAt(0)) == 'P') {
        this.title = "Performance";
        this.router.navigate(['/performance']);
    } 
      else {
        this.title = "Dashboard";
        this.router.navigate(['/dashboard']);
    } 
}

  logout() {
    alert('logged out');
  }
}
