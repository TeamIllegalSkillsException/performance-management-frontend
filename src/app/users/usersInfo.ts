export class UsersInfo {
    userId : number;
    firstName : string;
    lastName : string;
    emailAddress : string;
}