/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PerformanceComponent } from './performance.component';
import { DataCoinModule } from 'synerg-components';

describe('PerformanceComponent', () => {
  let component: PerformanceComponent;
  let fixture: ComponentFixture<PerformanceComponent>;
  let de: DebugElement[];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [DataCoinModule],
      declarations: [PerformanceComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerformanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain 2 data coin components', () => {
    de = fixture.debugElement.queryAll(By.css('.vdl-data-coin'));
    expect(de.length).toBe(2);
  });
});
