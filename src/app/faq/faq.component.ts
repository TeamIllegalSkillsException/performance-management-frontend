import { Component, OnInit } from '@angular/core';
import { FeedService } from './feed-service.service';
import { FeedEntry } from './model/feed-entry';

// Add the RxJS Observable operators we need in this app.
import './rxjs-operators';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {

  private feedUrl: string = 'http://blog.impraise.com/360-feedback?format=RSS';
  feeds: Array<FeedEntry> = [];

  constructor (
    private feedService: FeedService
  ) {}

  ngOnInit() {
    this.refreshFeed();
  }

  refreshFeed() {
    this.feeds.length = 0;
    console.log(this.feedUrl.length);
    // Adds 1s of delay to provide user's feedback.
    this.feedService.getFeedContent(this.feedUrl).delay(1000)
        .subscribe(
            feed => this.feeds = feed.items,
            error => console.log(error));
  }

}