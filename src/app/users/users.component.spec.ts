/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { UsersComponent } from './users.component';
import { DataCoinModule } from 'synerg-components';

describe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;
  let de: DebugElement[];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [DataCoinModule],
      declarations: [UsersComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain 2 data coin components', () => {
    de = fixture.debugElement.queryAll(By.css('.vdl-data-coin'));
    expect(de.length).toBe(2);
  });
});
