export class RatingDetails {
    userId: any;
    firstName: String;
    rating: any;

    constructor(userId : any,firstName : String,rating : any){
        this.userId = userId;
        this.firstName = firstName;
        this.rating = rating;
    }
}