import { Component, OnInit, HostListener } from '@angular/core';
import { IMastheadConfig, IMenuAction, INavigationItem } from 'synerg-components';
import { ActivatedRoute, Router } from '@angular/router';
import {AppComponent} from '../app.component';
import {Output, EventEmitter} from '@angular/core';
import { Injectable } from "@angular/core";

export const ANIMATIONS = {
  expand: 'expand',
  contract: 'contract'
};


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [AppComponent]
})

@Injectable()
export class loginComponent implements OnInit {
  public menuState: string = ANIMATIONS.contract;
  public actions: IMenuAction[];
  public navigation: INavigationItem[];
  public searchValue: string;
  public mastheadConfig: IMastheadConfig;
  public title: string;
  public items: any[];
constructor (private router: Router, private appcomponent:AppComponent) {}

  toggleMenu() {
    this.menuState = this.menuState === ANIMATIONS.contract ?
      ANIMATIONS.expand : ANIMATIONS.contract;
  }

  @HostListener('window:resize')
  onResize() {
    this.menuState = ANIMATIONS.contract;
  }

  @Output() public notifyParent: EventEmitter<any> = new EventEmitter<any>();
  
change() {
//this.notifyParent.subscribe(' To hide messages and alerts, omit from the mastheadConfig object');
this.router.navigate(['/dashboard']);
}
  ngOnInit() {
    this.title = 'Dashboard';
    // To hide messages and alerts, omit from the mastheadConfig object
    this.mastheadConfig = {
      avatar: {
        name: 'Daniel Reinke',
        imgPath: 'https://placeimg.com/80/80/people',
      },
      search: true,
      clientLogo: {
        type: 'icon',
        iconClass: 'icon-adp'
      },
      messages: 99,
      alerts: 1,
      actions: [
        {
          name: 'View Profile',
          icon: 'fa-user'
        },
        {
          name: 'Settings',
          icon: 'fa-gear'
        },
        {
          name: 'Tour',
          icon: 'fa-road'
        }
      ]
    };

    this.items = [
      {
        label: 'Dashboard',
        icon: 'tachometer',
        active: true
      },
      {
        label: 'Employees',
        icon: 'users',
        active: false
      },
      {
        label: 'Goals',
        icon: 'tasks',
        active: false
      }
    ];
  }

  mobileClick() {
    this.toggleMenu();
  }

  btnLogin(){

  }

  getAction(action: IMenuAction) {
    console.log(action.name + ' clicked.');
  }

  searchChange($event: Event) {
    let target = $event.target as HTMLInputElement;
    this.searchValue = target.value;
    console.log(this.searchValue);
  }

  searchClick() {
    alert('Search: ' + this.searchValue);
  }

  menuClick($event: Event) {
  
    let target = $event.target as HTMLInputElement;
    let value = target.value;
    let currentTarget = $event.currentTarget as HTMLOutputElement;
    console.log("currentTarget = " + currentTarget.innerText);
    
    if((currentTarget.innerText.charAt(0)) == 'E') {
        this.title = "Employees";
        this.router.navigate(['/users']);
    }
    else {
        this.title = "Dashboard";
        this.router.navigate(['/dashboard']);
    } 
}

  logout() {
    alert('logged out');
  }
}