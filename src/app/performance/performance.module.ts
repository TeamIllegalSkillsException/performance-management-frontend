import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SynergModule, TileModule } from 'synerg-components';
import { PerformanceComponent } from './performance.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { TabsModule, RadioModule, CheckboxModule, ButtonModule, SlideinModule } from 'synerg-components';
import { DropdownListModule } from 'synerg-components';
import { TextareaModule, ModalModule, SelectionListModule } from 'synerg-components';
import { BrowserModule } from '@angular/platform-browser';
import { ListViewModule } from 'synerg-components';
import { ChartModule } from 'angular2-highcharts';
import {HttpModule} from '@angular/http';
import {ModalComponent} from './modal.component';
import {Component, VERSION} from '@angular/core'

const routes: Routes = [{
  path: 'performance',
  component: PerformanceComponent
}];

@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    SynergModule,
    TileModule,
    RouterModule.forChild(routes),
    TabsModule,
    RadioModule,
    CheckboxModule,
    FormsModule,
    DropdownListModule,
    TextareaModule,
    ButtonModule,
    BrowserModule, 
    ReactiveFormsModule,
    ChartModule,
    ModalModule,
    SlideinModule,
    TileModule,
    ListViewModule,
    SelectionListModule,
    FormsModule, 
    HttpModule
    
  ],
  declarations: [PerformanceComponent,  ModalComponent ],
  bootstrap:    [ PerformanceComponent ],
  exports: [PerformanceComponent, RouterModule]
  
})
export class PerformanceModule { }
