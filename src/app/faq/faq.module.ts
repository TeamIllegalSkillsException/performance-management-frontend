import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// Material design.
import { MdButtonModule } from '@angular2-material/button';
import { MdCardModule } from '@angular2-material/card';
import { MdToolbarModule } from '@angular2-material/toolbar';
import { MdIconModule, MdIconRegistry } from '@angular2-material/icon';
//import { MdIconRegistry } from '@angular2-material/icon-registry';

import { FaqComponent } from './faq.component';
import { FeedCardComponent } from './feed-card/feed-card.component';
import { FeedService } from './feed-service.service';
import { StripHtmlTagsPipe } from './pipe/strip-html-tags.pipe';
import { SpinnerComponent } from './spinner/spinner.component';

const routes: Routes = [
  {
    path: 'faq',
    component: FaqComponent
  }
];

@NgModule({
  declarations: [
    FaqComponent,
    FeedCardComponent,
    StripHtmlTagsPipe,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MdCardModule,
    MdToolbarModule,
    MdButtonModule,
    MdIconModule,
    RouterModule.forRoot(routes)
  ],
  providers: [FeedService, MdIconRegistry],
  bootstrap: [FaqComponent]
})
export class FaqModule { }